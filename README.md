# PoC_Prometheus_Postgres

This repository allows to test exporters and alert manager for Prometheus integrated with Postgres. 


> Use official Docker images, avoid building images 
> Do not compile docker images unless strictly necessary


## For OnGres internal purposes only

[LT-47](https://issues.ongres.com/browse/LT-47)