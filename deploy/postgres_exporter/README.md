## Prometheus Postgres Exporter

This file is for PostgreSQL statistics exporting. Keep in mind any metrics exported from here will be:

a) exported from every database including read-only replicas
b) scraped frequently based on database-monitoring needs
c) Queried live on demand for every scrape

Moreover this file does not have tests and if you get this file
wrong you can easily disable all database monitoring metrics.


> Bellow comment is only related on to Gitlab setup, but we'll
> willing to add this to this PoC too

If you want to monitor the Gitlab system as a whole you're almost
certainly looking for gitlab-monitor:

https://gitlab.com/gitlab-org/gitlab-monitor

There you will be able to write Ruby code, cache data, access Redis,
etc.
